package com.epam.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
/**
 * Created by Mariya_Karatsuba on 3/17/2015.
 */
public class EmailNotSpam {
    //AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    //UI data
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By INBOX_BUTTON_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By SPAMBOX_BUTTON_LOCATOR = By.xpath("//a[@href='#spam']");
    public static final By CHECKBOX_BUTTON_LOCATOR = By.xpath("//input[@type ='checkbox']");
    public static final By NOTSPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='notspam']");

    //Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    //Test data
    private String userLogin = "karatsyuba.masha"; // ACCOUNT
    private String userPassword = "password1"; // PASSWORD

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void login () {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement spamboxButton = driver.findElement(SPAMBOX_BUTTON_LOCATOR);
        spamboxButton.click();
        WebElement checkboxButton = driver.findElement(CHECKBOX_BUTTON_LOCATOR);
        checkboxButton.click();
        WebElement notspamButton = driver.findElement(NOTSPAM_BUTTON_LOCATOR);
        notspamButton.click();
        WebElement inboxButton = driver.findElement(INBOX_BUTTON_LOCATOR);
        inboxButton.click();
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}
