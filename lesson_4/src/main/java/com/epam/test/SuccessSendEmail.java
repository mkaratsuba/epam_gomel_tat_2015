import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.ws.Dispatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mariya Karatsuba on 17.03.2015.
 */
public class SuccessSendEmail {

    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    // UI data
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//span[@class='b-toolbar__item__label js-toolbar-item-title-compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.xpath("//input[@id='compose-subj']");
    public static final By MAIL_TEXT_LOCATOR = By.xpath("//textarea[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By REFRESH_BUTTON_LOCATOR = By.xpath("//a[@class='b-stamp__mail daria-action']");
    public static final By DISPATCH_BUTTON_LOCATOR = By.xpath("//a[@href='#sent']");

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    private WebDriver driver;

    // Test data
    private String userLogin = "karatsyuba.masha"; // ACCOUNT
    private String userPassword = "password1"; // PASSWORD
    private String mailTo = "mariya_karatsuba@epam.com"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success mail login")
    public void login() {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        WebElement refreshButton = driver.findElement(REFRESH_BUTTON_LOCATOR);
        refreshButton.click();
        WebElement dispatchButton = driver.findElement(DISPATCH_BUTTON_LOCATOR);
        dispatchButton.click();
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}
